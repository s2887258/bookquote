package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn){
        return switch (isbn) {
            case "1" -> 10.0;
            case "2" -> 45.0;
            case "3" -> 20.0;
            case "4" -> 35;
            case "5" -> 50;
            default -> 0;
        };
    }
}
